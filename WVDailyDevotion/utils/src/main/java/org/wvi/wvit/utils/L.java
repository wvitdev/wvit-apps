package org.wvi.wvit.utils;

import android.util.Log;

/**
 * Utility class for Android logs.
 */
public final class L {
    public static final String TAG = "wvit-utils";

    public static final void d(String message) {
        d(TAG, message);
    }

    public static final void d(String TAG, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, message);
        }
    }

    public static final void e(String message) {
        e(TAG, message);
    }

    public static final void e(String TAG, String message) {
        if (BuildConfig.DEBUG) {
            Log.e(TAG, message);
        }
    }

    public static final void i(String message) {
        i(TAG, message);
    }

    public static final void i(String TAG, String message) {
        if (BuildConfig.DEBUG) {
            Log.i(TAG, message);
        }
    }
}
