package org.wvi.wvit.wvdailydevotion;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.wvi.wvit.bible.core.dailyverse.DailyVerse;
import org.wvi.wvit.bible.core.dailyverse.DailyVerseProvider;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NavigationView
        .OnNavigationItemSelectedListener, DailyVerseProvider.DailyVerseResponseListener {

    @BindView(R.id.main_drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.main_drawer)
    NavigationView drawer;

    @BindView(R.id.main_toolbar)
    Toolbar toolbar;

    @BindView(R.id.main_tv_daily_verse_text)
    AppCompatTextView scriptureTextView;

    @BindView(R.id.main_tv_daily_verse_reference)
    AppCompatTextView verseTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R
                .string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        drawer.setNavigationItemSelectedListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        DailyVerseProvider.getInstance().initialize(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        DailyVerseProvider.getInstance().setDailyVerseResponseListener(this);
        DailyVerseProvider.getInstance().getVerseOfTheDay();
    }

    @Override
    protected void onStop() {
        DailyVerseProvider.getInstance().stopRequestQueue();
        super.onStop();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.main_drawer_menu_bible) {
            Intent intent = new Intent(MainActivity.this, BibleActivity.class);
            startActivity(intent);
        }

        drawerLayout.closeDrawer(GravityCompat.START);
        return false;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(drawer)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDailyVerseRequestSuccess(DailyVerse dailyVerse) {
        scriptureTextView.setText(dailyVerse.getBibleScripture());
        verseTextView.setText(dailyVerse.getBibleVerse());
    }

    @Override
    public void onDailyVerseRequestError(String errorMessage) {
        verseTextView.setText(errorMessage);
    }
}
