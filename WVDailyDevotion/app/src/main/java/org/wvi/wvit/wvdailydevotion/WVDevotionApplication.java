package org.wvi.wvit.wvdailydevotion;

import android.app.Application;

import org.wvi.wvit.bible.core.BibleManager;

public class WVDevotionApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        BibleManager.getInstance().initialize(this);
    }
}
