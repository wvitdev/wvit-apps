package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
class Book {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long id;

    private String bookName;

    private int collection;

    Book(String bookName, int collection) {
        this.bookName = bookName;
        this.collection = collection;
    }

    void setId(@NonNull long id) {
        this.id = id;
    }

    @NonNull
    public long getId() {
        return id;
    }

    public String getBookName() {
        return bookName;
    }

    public int getCollection() {
        return collection;
    }
}
