package org.wvi.wvit.bible.core;

/**
 * Enum class to enumerate all books from the bible.
 */
public enum BookName {
    GENESIS("Genesis"),
    EXODUS("Exodus"),
    LEVITICUS("Leviticus"),
    NUMBERS("Numbers"),
    DEUTERONOMY("Deuteronomy"),
    MATTHEW("Matthew"),
    MARK("Mark"),
    LUKE("Luke"),
    JOHN("John");

    private String name;

    BookName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
