package org.wvi.wvit.bible.core.dailyverse;

import android.content.Context;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

public class DailyVerseProvider {

    private static DailyVerseProvider instance = null;

    private static final int CACHE_SIZE = 1024 * 1024; // 1MB

    private static final String REQUEST_URL = "https://beta.ourmanna.com/api/v1/get/?format=json";

    private static final String REQUEST_TAG = "WV_VERSE_OF_THE_DAY";

    private Context context;

    private DailyVerseResponseListener dailyVerseResponseListener;

    private Cache cache;

    private Network network;

    private RequestQueue requestQueue;

    private DailyVerseProvider() {

    }

    public static DailyVerseProvider getInstance() {
        if (null == instance) {
            instance = new DailyVerseProvider();
        }

        return instance;
    }

    public void initialize(Context context) {
        this.context = context;
        cache = new DiskBasedCache(context.getCacheDir(), CACHE_SIZE);
        network = new BasicNetwork(new HurlStack());
        requestQueue = new RequestQueue(cache, network);

        requestQueue.start();
    }

    public void setDailyVerseResponseListener(DailyVerseResponseListener listener) {
        this.dailyVerseResponseListener = listener;
    }

    public void stopRequestQueue() {
        if (requestQueue != null) {
            requestQueue.cancelAll(REQUEST_TAG);
        }
    }

    public void getVerseOfTheDay() throws NullPointerException {
        if (context == null) {
            throw new NullPointerException("Daily Verse Provider not initialized.");
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, REQUEST_URL,
                responseListener, errorListener);
        stringRequest.setTag(REQUEST_TAG);
        requestQueue.add(stringRequest);
    }

    private Response.Listener<String> responseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            DailyVerse dailyVerse = new Gson().fromJson(response, DailyVerse.class);
            dailyVerseResponseListener.onDailyVerseRequestSuccess(dailyVerse);
        }
    };

    private Response.ErrorListener errorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            dailyVerseResponseListener.onDailyVerseRequestError(error.getMessage());
        }
    };

    public interface DailyVerseResponseListener {

        void onDailyVerseRequestSuccess(DailyVerse dailyVerse);

        void onDailyVerseRequestError(String errorMessage);
    }
}
