package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;

@Dao
interface BibleDataAccess {

    @Insert
    long insertBook(Book book);

    @Insert
    long insertChapter(Chapter chapter);

    @Insert
    long insertVerse(Verse verse);

    @Query("SELECT * FROM Book WHERE bookName = :bookName")
    Book getBookByName(String bookName);

    @Query("SELECT * FROM Chapter WHERE bookId = :bookId AND chapterNumber = :chapterNumber")
    Chapter getChapter(long bookId, int chapterNumber);

    @Query("SELECT * FROM Verse WHERE chapterId = :chapterId AND verseNumber = :verseNumber")
    Verse getVerse(long chapterId, int verseNumber);
}
