package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = @ForeignKey(
                entity = Chapter.class,
                parentColumns = "id",
                childColumns = "chapterId",
                onDelete = CASCADE

        )
)
class Verse {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long id;

    private long chapterId;

    private int bibleVersion;

    private int verseNumber;

    private String verse;

    Verse(long chapterId, int bibleVersion, int verseNumber, String verse) {
        this.chapterId = chapterId;
        this.bibleVersion = bibleVersion;
        this.verseNumber = verseNumber;
        this.verse = verse;
    }

    void setId(@NonNull long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public long getChapterId() {
        return chapterId;
    }

    public int getBibleVersion() {
        return bibleVersion;
    }

    public int getVerseNumber() {
        return verseNumber;
    }

    public String getVerse() {
        return verse;
    }
}
