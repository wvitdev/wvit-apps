package org.wvi.wvit.bible.core;

/**
 * Enum class to indicate the version of the bible.
 */
public enum BibleVersion {
    NIV(0),
    ESV(1);

    private int versionId;

    BibleVersion(int versionId) {
        this.versionId = versionId;
    }

    @Override
    public String toString() {
        return versionId + "";
    }

    public int getVersionId() {
        return versionId;
    }
}
