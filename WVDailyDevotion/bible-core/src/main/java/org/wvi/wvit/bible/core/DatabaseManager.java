package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Room;
import android.content.Context;

class DatabaseManager {

    private static final String DB_NAME = "bible_db";

    private static DatabaseManager instance = null;

    private BibleDatabase database;

    private DatabaseManager() {

    }

    static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }

        return instance;
    }

    void initializeDatabase(Context context) {
        database = Room.databaseBuilder(context, BibleDatabase.class, DB_NAME)
                .allowMainThreadQueries().build();
    }

    long createBook(Book book) {
        return database.dataAccess().insertBook(book);
    }

    long createChapter(Chapter chapter) {
        return database.dataAccess().insertChapter(chapter);
    }

    long createVerse(Verse verse) {
        return database.dataAccess().insertVerse(verse);
    }

    Book getBook(String bookName) {
        return database.dataAccess().getBookByName(bookName);
    }

    Chapter getChapter(long bookId, int chapterNumber) {
        return database.dataAccess().getChapter(bookId, chapterNumber);
    }

    Verse getVerse(long chapterId, int verseNumber) {
        return database.dataAccess().getVerse(chapterId, verseNumber);
    }
}
