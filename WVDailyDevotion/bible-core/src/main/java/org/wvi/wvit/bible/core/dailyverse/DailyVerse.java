package org.wvi.wvit.bible.core.dailyverse;

public class DailyVerse {

    private Verse verse;

    public String getBibleScripture() {
        return verse.details.text;
    }

    public String getBibleVerse() {
        return verse.details.reference;
    }

    class Verse {

        private VerseDetails details;

        private String notice;

    }

    class VerseDetails {

        private String text;

        private String reference;

        private String version;

        private String verseurl;
    }
}
