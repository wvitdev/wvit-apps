package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = @ForeignKey(
                entity = Book.class,
                parentColumns = "id",
                childColumns = "bookId",
                onDelete = CASCADE
        )
)
class Chapter {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    private long id;

    private long bookId;

    private int chapterNumber;

    Chapter(long bookId, int chapterNumber) {
        this.bookId = bookId;
        this.chapterNumber = chapterNumber;
    }

    void setId(@NonNull long id) {
        this.id = id;
    }

    @NonNull
    public long getId() {
        return id;
    }

    public long getBookId() {
        return bookId;
    }

    public int getChapterNumber() {
        return chapterNumber;
    }
}
