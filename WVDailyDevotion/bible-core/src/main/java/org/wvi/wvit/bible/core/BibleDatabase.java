package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(
        entities = {
                Book.class,
                Chapter.class,
                Verse.class
        },
        version = 1
)
abstract class BibleDatabase extends RoomDatabase {
        abstract BibleDataAccess dataAccess();
}
