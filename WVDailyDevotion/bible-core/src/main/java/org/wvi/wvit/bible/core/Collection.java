package org.wvi.wvit.bible.core;

/**
 * Enum class to indicate if a book is from the old testament or the new testament.
 */
public enum Collection {
    OLD_TESTAMENT(0),
    NEW_TESTAMENT(1);

    private int collectionId;

    Collection(int collectionId) {
        this.collectionId = collectionId;
    }

    @Override
    public String toString() {
        return collectionId + "";
    }

    public int getCollectionId() {
        return collectionId;
    }
}
