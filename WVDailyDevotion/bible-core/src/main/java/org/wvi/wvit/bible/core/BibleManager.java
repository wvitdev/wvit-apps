package org.wvi.wvit.bible.core;

import android.arch.persistence.room.Database;
import android.content.Context;

public class BibleManager {

    private static BibleManager instance = null;

    private BibleManager() {

    }

    public static BibleManager getInstance() {
        if (null == instance) {
            instance = new BibleManager();
        }

        return instance;
    }

    public void initialize(Context context) {
        DatabaseManager.getInstance().initializeDatabase(context);
        BibleBuilder.buildBible(BibleVersion.NIV);

        // TODO other initializations

        // TODO one-time build from JSON asset
    }

    public BibleScripture getBibleScripture(BookName bookName, int chapterNumber, int verseNumber) {
        Book book = DatabaseManager.getInstance().getBook(bookName.toString());
        Chapter chapter = DatabaseManager.getInstance().getChapter(book.getId(), chapterNumber);
        Verse verse = DatabaseManager.getInstance().getVerse(chapter.getId(), verseNumber);

        BibleScripture scripture = new BibleScripture(book.getBookName(), chapter
                .getChapterNumber(), verse.getVerseNumber(), verse.getVerse());
        return scripture;
    }
}
