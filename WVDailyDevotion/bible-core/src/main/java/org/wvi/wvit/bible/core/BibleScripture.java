package org.wvi.wvit.bible.core;

public class BibleScripture {

    private String bookName;

    private int chapter;

    private int verse;

    private String scripture;

    BibleScripture(String bookName, int chapter, int verse, String scripture) {
        this.bookName = bookName;
        this.chapter = chapter;
        this.verse = verse;
        this.scripture = scripture;
    }

    public String getBookName() {
        return bookName;
    }

    void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getChapter() {
        return chapter;
    }

    void setChapter(int chapter) {
        this.chapter = chapter;
    }

    public int getVerse() {
        return verse;
    }

    void setVerse(int verse) {
        this.verse = verse;
    }

    public String getScripture() {
        return scripture;
    }

    void setScripture(String scripture) {
        this.scripture = scripture;
    }

    public String getBibleRefrence() {
        return bookName + " " + chapter + ":" + verse;
    }
}
